//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	WIDTH				= 800;
const	HEIGHT				= 800;
const	PARALLAX_RATIO		= 1.5;
const	STEP				= 1.3;
const	PARTICLE_THRESOLD	= 10;
const	FRAME_CYCLE			= 6;

let		g_shapes			= null;
let		g_frame				= 0;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// TOOLS
//////////////////////////////////////////////////

function	draw_particles(particles, x, y, w) {
	let		particle;

	fill(255);
	for (particle of particles) {
		ellipse(x + (w * particle.x), y + (w * particle.y), 5);
	}
}

//////////////////////////////////////////////////
/// P5 MAIN
//////////////////////////////////////////////////

function	setup() {
	let		i;

	createCanvas(WIDTH, HEIGHT);
	g_shapes = [];
	mouseX = WIDTH / 2;
	mouseY = HEIGHT / 2;
	noStroke();
}

function	draw() {
	let		i;
	let		x, y, w;
	let		shape;
	let		parallax;

	background(0, 0, 0, 255);
	g_shapes.push({
		"x": mouseX,
		"y": mouseY,
		"w": 5,
		"particles": []
	});
	/// ADD PARTICLES
	if (random() > PARTICLE_THRESOLD) {
		g_shapes[g_shapes.length - 1]["particles"].push({
			"x": random(),
			"y": random()
		});
	}
	/// UPDATE SHAPES
	if (g_frame == 0) {
		for (i = 0; i < g_shapes.length; ++i) {
			shape = g_shapes[i];
			/// UPDATE POSITIONS
			if (i < g_shapes.length - 1) {
				shape.x = g_shapes[i + 1].x;
				shape.y = g_shapes[i + 1].y;
			}
			/// UPDATE WIDTH
			shape.w *= STEP;
		}
	}
	/// REMOVE BIG SHAPES
	while (g_shapes[0].w > WIDTH * PARALLAX_RATIO) {
		g_shapes.shift();
	}
	stroke(255);
	/// PRINT SHAPES
	for (i = g_shapes.length - 1; i >= 0; --i) {
		shape = g_shapes[i];
		w = shape.w * (1 + (STEP - 1) * (g_frame / FRAME_CYCLE));
		parallax = max(0, 1 - (w / (WIDTH * PARALLAX_RATIO)));
		x = WIDTH / 2 + ((shape.x - (WIDTH / 2)) * parallax) - (w / 2);
		y = HEIGHT / 2 + ((shape.y - (HEIGHT / 2)) * parallax) - (w / 2);
		noFill();
		rect(x, y, w, w);
		draw_particles(shape.particles, x, y, w);
	}
	/// HANDLE ANIMATION FRAME
	if (++g_frame >= FRAME_CYCLE) {
		g_frame = 0;
	}
}
